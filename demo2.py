import random

data = random.randint(0, 50)
count = 0
gold = 100
while gold <= 100:
    num = int(input("please enter a number:"))
    gold = gold - 20
    count += 1
    print("You have",gold,"coin(s) left")
    if gold > 0:
        if num > data:
            print("too big")
        elif num < data:
            print("too small")
        elif num == data:
            print("You are right!", data, ".", "You have used", count, "time(s) in total")
            break
    elif gold<=0:
        print("You have run out of coins")
        break
